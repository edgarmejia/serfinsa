package com.redserfinsa.ui

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.redserfinsa.R
import com.redserfinsa.databinding.ActivityLoginBinding
import com.redserfinsa.ui.commerce.CommerceActivity
import com.redserfinsa.ui.shared.LoadingDialog

class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    private val firebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    private val loadingDialog by lazy {
        LoadingDialog(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()
    }

    private fun init() {
        binding.apply {
            btnLogin.setOnClickListener { doLoginByEmail() }
            btnLoginWithGoogle.setOnClickListener { doLoginWithGoogle() }
        }
    }

    private fun getEmail(): String = binding.emailField.text.toString()

    private fun getPwd(): String = binding.passwordField.text.toString()

    private fun validateLoginForm(): Boolean {
        return when {
            (getEmail().isEmpty() && getPwd().isEmpty()) -> setEmailAndPwdFieldsErrors()
            getEmail().isEmpty() -> setEmailFieldError()
            getPwd().isEmpty() -> setPwdFieldError()
            else -> true
        }
    }

    private fun setEmailAndPwdFieldsErrors(): Boolean {
        setEmailFieldError()
        setPwdFieldError()
        return false
    }

    private fun setEmailFieldError(): Boolean {
        binding.emailField.error = getString(R.string.generic_validator_message)
        binding.emailField.requestFocus()
        return false
    }

    private fun setPwdFieldError(): Boolean {
        binding.passwordField.error = getString(R.string.generic_validator_message)
        binding.passwordField.requestFocus()
        return false
    }

    private fun makeSession(sessionTask: Task<AuthResult>) {
        when {
            sessionTask.isSuccessful -> moveToHome(sessionTask.result?.user?.email ?: "")
            else -> Toast.makeText(this, getString(R.string.session_error_message), Toast.LENGTH_LONG).show()
        }
    }

    private fun doLoginByEmail() {
        if (!validateLoginForm()) return
        loadingDialog.show()

        firebaseAuth
            .signInWithEmailAndPassword(getEmail(), getPwd())
            .addOnCompleteListener {
                loadingDialog.dismiss()
                makeSession(it)
            }.addOnFailureListener { loadingDialog.dismiss() }
    }

    private fun doLoginWithGoogle() {
        val googleConfig: GoogleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build()

        val googleClient: GoogleSignInClient = GoogleSignIn.getClient(this, googleConfig)
        loginWithGoogleLauncher.launch(googleClient.signInIntent)
    }

    private val loginWithGoogleLauncher = registerForActivityResult(
        ActivityResultContracts.StartActivityForResult()
    ) { result ->
        if (result.resultCode == Activity.RESULT_OK) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(result.data)
            handleLoginResults(task)
        }
    }

    private fun handleLoginResults(task: Task<GoogleSignInAccount>) {
        when {
            !task.isSuccessful -> Toast.makeText(this, getString(R.string.session_error_message), Toast.LENGTH_LONG).show()
            (task.isSuccessful && task.result != null) -> {
                try {
                    val account = task.result
                    val credentials = GoogleAuthProvider.getCredential(account.idToken, null)

                    firebaseAuth
                        .signInWithCredential(credentials)
                        .addOnCompleteListener { makeSession(it) }
                } catch (e: ApiException) {
                    Log.d(TAG_ERROR_GOOGLE_LOGIN_RESULT, e.toString())
                }
            }
        }
    }

    private fun moveToHome(email: String) {
        val homeIntent = Intent(this, CommerceActivity::class.java).apply {
            putExtra(INTENT_EXTRAS_EMAIL, email)
        }
        startActivity(homeIntent)
    }

    companion object {
        const val INTENT_EXTRAS_EMAIL = "email"
        const val TAG_ERROR_GOOGLE_LOGIN_RESULT = "handleResults()"
    }
}
