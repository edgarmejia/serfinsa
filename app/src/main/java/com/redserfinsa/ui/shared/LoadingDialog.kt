package com.redserfinsa.ui.shared

import android.app.Activity
import android.app.AlertDialog
import com.redserfinsa.R

class LoadingDialog(private val activity: Activity) {

    private lateinit var isDialog: AlertDialog

    fun show() {
        val inflater = activity.layoutInflater
        val dialogView = inflater.inflate(R.layout.item_loading, null)

        isDialog = AlertDialog.Builder(activity).apply {
            setView(dialogView)
            setCancelable(false)
        }.create()

        isDialog.show()
    }

    fun dismiss() {
        isDialog.dismiss()
    }

}