package com.redserfinsa.ui.commerce

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.redserfinsa.R
import com.redserfinsa.databinding.ActivityCommerceBinding

class CommerceActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCommerceBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityCommerceBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, CommerceListFragment())
            .commit()

        init()
    }

    private fun init() {
        binding.btnAddCommerce.setOnClickListener { showAddCommerceForm() }
    }

    private fun showAddCommerceForm() = AddCommerceDialog()
        .show(supportFragmentManager, AddCommerceDialog.TAG)

}