package com.redserfinsa.ui.commerce

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.redserfinsa.R
import com.redserfinsa.data.CommerceDto
import com.redserfinsa.databinding.DialogAddCommerceBinding

class AddCommerceDialog : DialogFragment() {

    private var _binding: DialogAddCommerceBinding? = null

    private val binding get() = _binding!!

    private val fireStore by lazy {
        FirebaseFirestore.getInstance()
    }

    private val firebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    override fun onCreateDialog(
        savedInstanceState: Bundle?
    ): Dialog {
        _binding = DialogAddCommerceBinding
            .inflate(LayoutInflater.from(context))

        val dialog = AlertDialog
            .Builder(requireActivity())
            .setView(binding.root)
            .create()

        dialog.window!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        initBindings()
        return dialog
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initBindings() {
        binding.apply {
            btnAddCommerce.setOnClickListener { validateForm() }
            btnClose.setOnClickListener { dismiss() }
        }
    }

    private fun setFormEnable(isEnable: Boolean) {
        binding.apply {
            btnClose.isEnabled = isEnable

            fieldCommerceName.isEnabled = isEnable
            fieldCommerceName.isFocusableInTouchMode = isEnable
            fieldCommerceName.isFocusable = isEnable

            fieldCommerceState.isEnabled = isEnable
            fieldCommerceState.isFocusableInTouchMode = isEnable
            binding.fieldCommerceState.isFocusable = isEnable

            fieldCommerceDocumentNumber.isEnabled = isEnable
            fieldCommerceDocumentNumber.isFocusableInTouchMode = isEnable
            fieldCommerceDocumentNumber.isFocusable = isEnable
        }
    }

    private fun validateForm() {
        val fieldCommerceName = binding.fieldCommerceName.text.toString()
        val fieldCommerceState = binding.fieldCommerceState.text.toString()
        val fieldCommerceDocumentNumber = binding.fieldCommerceDocumentNumber.text.toString()

        when {
            fieldCommerceName.isEmpty() -> {
                binding.fieldCommerceName.error = getString(R.string.generic_validator_message)

                binding.fieldCommerceName.requestFocus()
            }
            fieldCommerceState.isEmpty() -> {
                binding.fieldCommerceState.error = getString(R.string.generic_validator_message)
                binding.fieldCommerceState.requestFocus()
            }
            fieldCommerceDocumentNumber.isEmpty() -> {
                binding.fieldCommerceDocumentNumber.error = getString(R.string.generic_validator_message)
                binding.fieldCommerceDocumentNumber.requestFocus()
            }
            else -> callAddCommerce(
                name = fieldCommerceName,
                state = fieldCommerceState,
                documentNumber = fieldCommerceDocumentNumber
            )
        }
    }

    private fun callAddCommerce(name: String, state: String, documentNumber: String) {
        setFormEnable(isEnable = false)

        fireStore.collection(COLLECTION_COMMERCE)
            .add(CommerceDto(
                id = firebaseAuth.currentUser?.uid,
                name,
                state,
                documentNumber
            ))
            .addOnSuccessListener {
                showToast(getString(R.string.add_commerce_success))
                dismiss()
            }
            .addOnFailureListener {
                showToast(getString(R.string.add_commerce_error))
                setFormEnable(isEnable = true)
            }
    }

    private fun showToast(message: String) {
        Toast.makeText(
            requireContext(),
            message,
            Toast.LENGTH_LONG
        ).show()
    }

    companion object {
        const val TAG = "AddCommerceDialog"
        const val COLLECTION_COMMERCE = "Commerce"
    }

}