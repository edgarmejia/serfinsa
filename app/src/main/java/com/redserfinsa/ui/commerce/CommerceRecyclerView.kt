package com.redserfinsa.ui.commerce

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.redserfinsa.data.CommerceItem
import com.redserfinsa.databinding.FragmentItemBinding

class CommerceRecyclerView(
    private val values: MutableList<CommerceItem>
) : RecyclerView.Adapter<CommerceRecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        holder.idName.text = item.name
        holder.idState.text = item.state
        holder.idDocument.text = item.documentNumber
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentItemBinding) : RecyclerView.ViewHolder(binding.root) {
        val idName: TextView = binding.textName
        val idState: TextView = binding.textState
        val idDocument: TextView = binding.textDocument
    }

}