package com.redserfinsa.ui.commerce

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.redserfinsa.R
import com.redserfinsa.data.CommerceItem
import com.redserfinsa.ui.shared.LoadingDialog

class CommerceListFragment : Fragment() {

    private var columnCount = 1

    lateinit var adapterCommerces: CommerceRecyclerView

    private val fireStore by lazy {
        FirebaseFirestore.getInstance()
    }

    private val firebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    private val loadingDialog by lazy {
        LoadingDialog(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        loadingDialog.show()
        val values: MutableList<CommerceItem> = arrayListOf()

        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }

                fireStore.collection(KEY_COMMERCE_COLLECTION)
                    .get()
                    .addOnSuccessListener { result ->
                        var i = 1
                        for (document in result) {
                            if (document.data.values.elementAt(2).toString() == firebaseAuth.currentUser?.uid.toString()) {
                                values.add(
                                    CommerceItem(
                                        id = i.toString(),
                                        documentNumber = document.data.values.elementAt(0).toString(),
                                        name = document.data.values.elementAt(1).toString(),
                                        state = document.data.values.elementAt(3).toString(),
                                    )
                                )
                                i += 1
                            }
                        }

                        adapter = CommerceRecyclerView(values)
                        adapterCommerces = adapter as CommerceRecyclerView

                        loadingDialog.dismiss()
                    }
                    .addOnFailureListener { exception ->
                        Log.d(TAG_ERROR, "Error getting documents.", exception)
                        loadingDialog.dismiss()
                    }
            }
        }

        return view
    }

    companion object {
        const val TAG_ERROR = "getting commerces"
        const val KEY_COMMERCE_COLLECTION = "Commerce"
    }

}