package com.redserfinsa.data

data class CommerceDto(
    val id: String?,
    val name: String,
    val state: String,
    val documentNumber: String,
)
