package com.redserfinsa.data

data class CommerceItem(
    val id: String,
    val name: String,
    val state: String,
    val documentNumber: String,
)
