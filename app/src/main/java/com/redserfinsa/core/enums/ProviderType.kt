package com.redserfinsa.core.enums

enum class ProviderType {
    EMAIL,
    GOOGLE
}